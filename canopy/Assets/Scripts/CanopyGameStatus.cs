﻿using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;
using Newtonsoft.Json;
using System.Collections.Generic;

public class CanopyGameStatus
{
    public static List<Seasons> seasons = new List<Seasons>();
    public static int WeeksInMonth = 0;
    public static int DaysInWeek = 0;
    public static List<String> DaysName = new List<String>();
    public static CanopyGameTime gameTime = new CanopyGameTime();

    public static DateTime lastTimeSnapshot = new DateTime();
    public static int gameHour = 0;
    public static int gameMinute = 0;
    public static bool globalsInitialized = false;

    public static Queue<string> Dialog = new Queue<string>();

    public static void initializeGlobals()
    {
        if (!globalsInitialized)
        {
            StreamReader canopyJsonFile;
            String canopyJsonData;

            try
            {
                canopyJsonFile = new StreamReader(Application.dataPath + "/Config/canopy.json");
                canopyJsonData = canopyJsonFile.ReadToEnd();
                CanopyGameStatusStructure cgStruct = new CanopyGameStatusStructure();
                cgStruct = JsonConvert.DeserializeObject<CanopyGameStatusStructure>(canopyJsonData);
                //cgStruct.print();
                cgStruct.setToGlobal();
                globalsInitialized = true;

            }
            catch (Exception e)
            {
                Debug.Log(e);
            }
        }

    }

    public static void addDialog(string newDialog)
    {
        Debug.Log("Enqueing Dialog : " + newDialog);
        Dialog.Enqueue(newDialog);
    }

}