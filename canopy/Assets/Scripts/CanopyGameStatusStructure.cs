﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class Seasons
{
    public string Name { get; set; }
    public string SourceFile { get; set; }
    public int Order { get; set; }
    public int Months { get; set; }
}

public class CanopyGameTime
{
    public int HoursInDay { get; set; }
    public int WakeUp { get; set; }
    public int DinnerTime { get; set; }
    public int Evening { get; set; }
    public int DeadTime { get; set; }
    public int RealToGame { get; set; }
}

public class CanopyGameStatusStructure
{
    public List<Seasons> seasons;
    public int WeeksInMonth;
    public int DaysInWeek;
    public List<String> DaysName;
    public CanopyGameTime gameTime;

    public CanopyGameStatusStructure()
    {
        seasons = new List<Seasons>();
        gameTime = new CanopyGameTime();
        WeeksInMonth = 0;
        DaysInWeek = 0;
    }

    public void print()
    {
        Debug.Log("Seasons : ");
        foreach (var item in this.seasons)
        {
            Debug.Log("Name : " + item.Name);
            Debug.Log("SourceFile : " + item.SourceFile);
            Debug.Log("Order : " + item.Order);
            Debug.Log("Months : " + item.Months);
        }
        Debug.Log("WeeksInMonth : " + this.WeeksInMonth);
        Debug.Log("DaysInWeek : " + this.DaysInWeek);
        foreach (var item in this.DaysName)
        {
            Debug.Log("DayName : " + item);
        }

        Debug.Log("HoursInDay : " + this.gameTime.HoursInDay);
        Debug.Log("WakeUp : " + this.gameTime.WakeUp);
        Debug.Log("DinnerTime : " + this.gameTime.DinnerTime);
        Debug.Log("Evening : " + this.gameTime.Evening);
        Debug.Log("DeadTime : " + this.gameTime.DeadTime);
        Debug.Log("RealToGame : " + this.gameTime.RealToGame);
    }

    public void setToGlobal()
    {
        foreach (var item in this.seasons)
        {
            CanopyGameStatus.seasons.Add(item);
        }
        CanopyGameStatus.WeeksInMonth = this.WeeksInMonth;
        CanopyGameStatus.DaysInWeek = this.DaysInWeek;
        foreach (var item in this.DaysName)
        {
            CanopyGameStatus.DaysName.Add(item);
        }

        CanopyGameStatus.gameTime.HoursInDay = this.gameTime.HoursInDay;
        CanopyGameStatus.gameTime.WakeUp = this.gameTime.WakeUp;
        CanopyGameStatus.gameTime.DinnerTime = this.gameTime.DinnerTime;
        CanopyGameStatus.gameTime.Evening = this.gameTime.Evening;
        CanopyGameStatus.gameTime.DeadTime = this.gameTime.DeadTime;
        CanopyGameStatus.gameTime.RealToGame = this.gameTime.RealToGame;

    }
}