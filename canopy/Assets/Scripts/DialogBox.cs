using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class DialogBox : MonoBehaviour
{
    private TMPro.TextMeshProUGUI mainDialogBox;

    // Start is called before the first frame update
    void Start()
    {
        mainDialogBox = GetComponent<TMPro.TextMeshProUGUI>();
        Debug.Log("Default text is : " + mainDialogBox);
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log("Dialog Queue Count : " + CanopyGameStatus.Dialog.Count);
        if(CanopyGameStatus.Dialog.Count == 0)
        {
            return;
        }

        string dialog = CanopyGameStatus.Dialog.Dequeue();
        StopAllCoroutines();
        StartCoroutine(TypeSentence(dialog));
        CanopyGameStatus.Dialog.Clear();

    }

    IEnumerator TypeSentence (string sentence)
    {
        mainDialogBox.text = "";
        foreach (char letter in sentence.ToCharArray())
        {
            mainDialogBox.text += letter;
            yield return null;
        }
    }


}
