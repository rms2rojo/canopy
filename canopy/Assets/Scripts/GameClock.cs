using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GameClock : MonoBehaviour
{
    private TMPro.TextMeshProUGUI textClock;

    int increaseGameClockMinutesSeconds(DateTime time)
    {
        TimeSpan diffTime = time - CanopyGameStatus.lastTimeSnapshot;

        //if a minute has passed, increase game clock by 10 minutes
        if (diffTime.Seconds >= 10)
        {
            CanopyGameStatus.lastTimeSnapshot = DateTime.Now;
            return CanopyGameStatus.gameTime.RealToGame;
        }

        return 0;
    }

    // Start is called before the first frame update
    void Start()
    {
        textClock = GetComponent<TMPro.TextMeshProUGUI>();
        CanopyGameStatus.lastTimeSnapshot = DateTime.Now;
        CanopyGameStatus.gameHour = CanopyGameStatus.gameTime.WakeUp;
    }

    // Update is called once per frame
    void Update()
    {
        CanopyGameStatus.gameMinute += increaseGameClockMinutesSeconds(DateTime.Now);

        //if game has passed an hour, reset minutes and increase hour
        if(CanopyGameStatus.gameMinute >= 60)
        {
            CanopyGameStatus.gameMinute = 0;
            CanopyGameStatus.gameHour++;
            if(CanopyGameStatus.gameHour == CanopyGameStatus.gameTime.HoursInDay)
            {
                CanopyGameStatus.gameHour = 0;
            }
        }

        string hour = LeadingZero(CanopyGameStatus.gameHour);
        string minute = LeadingZero(CanopyGameStatus.gameMinute);
        textClock.text = hour + ":" + minute;
    }

    string LeadingZero(int n)
    {
        return n.ToString().PadLeft(2, '0');
    }


    
}
