using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;
using Newtonsoft.Json;
using System.Collections.Generic;


public class NPCPlayerController : MonoBehaviour
{
    public String characterName;

    List<NPCSchedule> schedule;
    List<NPCDialog> dialog;

    // Start is called before the first frame update
    void Start()
    {

        StreamReader scheduleJsonFile;
        String scheduleJsonData;
        StreamReader dialogJsonFile;
        String dialogJsonData;

        try
        {
            scheduleJsonFile = new StreamReader(Application.dataPath + "/Config/" + characterName + "NPCSchedule.json");
            scheduleJsonData = scheduleJsonFile.ReadToEnd();
            if(schedule == null)
            {
                schedule = new List<NPCSchedule>();
            }
                
            schedule = JsonConvert.DeserializeObject<List<NPCSchedule>>(scheduleJsonData);
            printSchedule();
        }
        catch (Exception e)
        {
            Debug.Log(e);
        }

        try
        {
            dialogJsonFile = new StreamReader(Application.dataPath + "/Config/" + characterName + "NPCDialog.json");
            dialogJsonData = dialogJsonFile.ReadToEnd();
            if (dialog == null)
            {
                dialog = new List<NPCDialog>();
            }

            dialog = JsonConvert.DeserializeObject<List<NPCDialog>>(dialogJsonData);
            //printDialog();
        }
        catch (Exception e)
        {
            Debug.Log(e);
        }
    }

    // Update is called once per frame
    void Update()
    {

        NPCSchedule frameSchedule = checkSchedule();
        

        if (frameSchedule != null)
        {
            if(frameSchedule.DialogKey >= 0)
            {
                string NameAndDialog = characterName + " says : " + dialog[frameSchedule.DialogKey].Words;
                CanopyGameStatus.addDialog(NameAndDialog);
            }
        }

    }

    NPCSchedule checkSchedule()
    {
        foreach (var item in this.schedule)
        {

            if (item.GameHour == CanopyGameStatus.gameHour &&
               item.GameMinute == CanopyGameStatus.gameMinute &&
               !item.DoneToday)
            {
                item.DoneToday = true;
                return (item);
            }
        }

        return null;
    }



    public void printSchedule()
    {
        Debug.Log("NPC Schedule : ");
        foreach (var item in this.schedule)
        {
            Debug.Log("Key : " + item.Key);
            Debug.Log("Season : " + item.Season);
            Debug.Log("GameHour  : " + item.GameHour);
            Debug.Log("GameMinute  : " + item.GameMinute);
            Debug.Log("MapName  : " + item.MapName);
            Debug.Log("LocationX  : " + item.LocationX);
            Debug.Log("LocationY  : " + item.LocationY);
            Debug.Log("Animation  : " + item.Animation);
            Debug.Log("DialogKey  : " + item.DialogKey);
            Debug.Log("DoneToday  : " + item.DoneToday);
        }
    }

    public void printDialog()
    {
        Debug.Log("NPC Dialog : ");
        foreach (var item in this.dialog)
        {
            Debug.Log("Key : " + item.Key);
            Debug.Log("Chance : " + item.Chance);
            Debug.Log("Words  : " + item.Words);
        }
    }

}

public class NPCSchedule
{
    public int Key { get; set; }
    public int Season { get; set; }
    public int GameHour { get; set; }
    public int GameMinute { get; set; }
    public string MapName { get; set; }
    public int LocationX { get; set; }
    public int LocationY { get; set; }
    public string Animation { get; set; }
    public int DialogKey { get; set; }
    public bool DoneToday { get; set; }
}

public class NPCDialog
{
    public int Key { get; set; }
    public int Chance { get; set; }
    public string Words { get; set; }

}
