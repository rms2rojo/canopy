using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float basicSpeed = 1;
    public int changeSpeed = 10000;
    public Rigidbody2D rb;
    Vector2 movement;
    public Animator animator;
    //int counter = 0;

    private void Start()
    {
        CanopyGameStatus.initializeGlobals();
    }

    // Update is called once per frame
    void Update()
    {
        //input
        movement.x = Input.GetAxisRaw("Horizontal");
        movement.y = Input.GetAxisRaw("Vertical");

        rb.velocity = new Vector2(movement.x, movement.y);

        animator.SetFloat("Horizontal", movement.x);
        animator.SetFloat("Vertical", movement.y);
        animator.SetFloat("Speed", movement.sqrMagnitude);


    }

    //This is called at fixed time interval
    void FixedUpdate()
    {
        //movement
        rb.MovePosition(rb.position + movement * basicSpeed * Time.fixedDeltaTime);

    }
}
