<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.5" tiledversion="1.6.0" name="CollisionTiles" tilewidth="16" tileheight="16" tilecount="8" columns="8">
 <image source="CollisionTiles.png" width="128" height="16"/>
 <tile id="0">
  <objectgroup draworder="index" id="4">
   <object id="3" type="Collision" x="1" y="1" width="14" height="14"/>
  </objectgroup>
 </tile>
</tileset>
