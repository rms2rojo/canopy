<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.5" tiledversion="1.6.0" name="PathTiles" tilewidth="16" tileheight="16" tilecount="8" columns="8">
 <image source="PathTiles.png" width="128" height="16"/>
 <tile id="0">
  <properties>
   <property name="SpawnVine" type="bool" value="true"/>
  </properties>
 </tile>
</tileset>
