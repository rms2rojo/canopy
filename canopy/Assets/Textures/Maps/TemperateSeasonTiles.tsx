<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.5" tiledversion="1.6.0" name="TemperateSeasonTiles" tilewidth="16" tileheight="16" tilecount="96" columns="8">
 <image source="TemperateSeasonTiles.png" width="128" height="192"/>
 <tile id="4" probability="0.01"/>
 <tile id="5" probability="0.01"/>
 <tile id="6" probability="0.01"/>
 <tile id="7" probability="0.01"/>
 <tile id="8" probability="0.01"/>
 <tile id="9" probability="0.01"/>
 <tile id="10" probability="0.01"/>
 <tile id="11" probability="0.01"/>
 <tile id="36">
  <animation>
   <frame tileid="36" duration="1200"/>
   <frame tileid="38" duration="1200"/>
  </animation>
 </tile>
 <tile id="37">
  <animation>
   <frame tileid="37" duration="1200"/>
   <frame tileid="39" duration="1200"/>
  </animation>
 </tile>
 <tile id="46">
  <animation>
   <frame tileid="46" duration="1200"/>
   <frame tileid="54" duration="1200"/>
  </animation>
 </tile>
 <tile id="47">
  <animation>
   <frame tileid="47" duration="1200"/>
   <frame tileid="55" duration="1200"/>
  </animation>
 </tile>
 <tile id="58">
  <animation>
   <frame tileid="58" duration="1200"/>
   <frame tileid="61" duration="1200"/>
  </animation>
 </tile>
 <tile id="59">
  <animation>
   <frame tileid="59" duration="1200"/>
   <frame tileid="60" duration="1200"/>
  </animation>
 </tile>
 <tile id="68">
  <animation>
   <frame tileid="68" duration="400"/>
   <frame tileid="69" duration="400"/>
   <frame tileid="70" duration="400"/>
   <frame tileid="71" duration="400"/>
   <frame tileid="76" duration="400"/>
   <frame tileid="77" duration="400"/>
   <frame tileid="78" duration="400"/>
   <frame tileid="79" duration="400"/>
  </animation>
 </tile>
</tileset>
